﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Xml.Serialization;
using MyDesigner.HelperControls;
using MyDesigner.Shapes;

namespace MyDesigner
{
    public class MyCanvasModel
    {
        [XmlIgnore]
        public ModelCommand DeleteCommand { get; set; }
        [XmlIgnore]
        public ModelCommand ChangeTextCommand { get; set; }
        [XmlIgnore]
        public ModelCommand SaveRavenCommand { get; set; }
        [XmlIgnore]
        public ModelCommand LoadRavenCommand { get; set; }
        [XmlIgnore]
        public ModelCommand SaveXmlCommand { get; set; }
        [XmlIgnore]
        public ModelCommand LoadXmlCommand { get; set; }

        public MyCanvasModel()
        {
            Shapes = new List<MyShape>();
            Lines = new List<MyLine>();

            DeleteCommand = new ModelCommand(() =>
            {
                if (CurrentElement != null)
                {
                    CurrentElement.Delete(this);
                }
            });
            ChangeTextCommand = new ModelCommand(() =>
            {
                if (CurrentElement is MyShape)
                {
                    var shape = CurrentElement.GetShape() as ShapeContainer;
                    shape.EditText();
                }
            });
        }

        [XmlIgnore]
        public Tools CurrentTool { get; set; }

        private BaseShape _currentElement ;
        [XmlIgnore]
        public BaseShape CurrentElement {
            get
            {
                return _currentElement;
            }
            set
            {
                if (_currentElement == value) return;

                Shapes.ForEach(s => s.IsSelected = false);
                Lines.ForEach(l => { l.StartConnector.IsSelected = false; l.EndConnector.IsSelected = false; });

                _currentElement = value;

                if (_currentElement != null)
                    _currentElement.IsSelected = true;
            }
        }

        public List<MyShape> Shapes { get; set; }
        public List<MyLine> Lines { get; set; }
    }

    public class ModelCommand: ICommand
    {
        private readonly Action _action;

        public ModelCommand(Action action)
        {
            _action = action;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
