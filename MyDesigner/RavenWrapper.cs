﻿using System.Collections.Generic;
using System.Linq;
using Raven.Client.Embedded;

namespace MyDesigner
{
    public class RavenWrapper
    {
        private static EmbeddableDocumentStore _documentStore;
        private static EmbeddableDocumentStore DocumentStore
        {
            get
            {
                if (_documentStore == null)
                {
                    _documentStore = new EmbeddableDocumentStore
                    {
                        DataDirectory = "Data"
                    };

                    _documentStore.Initialize();
                }

                return _documentStore;
            }
        }
        
        public void Save(MyCanvasModel model)
        {
            using (var session = DocumentStore.OpenSession())
            {
                foreach (var element in model.Shapes)
                {
                    session.Store(element);
                }
                foreach (var line in model.Lines)
                {
                    session.Store(line);
                }

                session.SaveChanges();
            }
        }

        public List<T> Load<T>()
        {
            using (var session = DocumentStore.OpenSession())
            {
                return session.Query<T>().ToList();
            }
        }
    }
}
