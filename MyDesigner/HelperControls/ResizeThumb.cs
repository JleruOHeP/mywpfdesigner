﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace MyDesigner.HelperControls
{
    public class ResizeThumb : Thumb
    {
        public ResizeThumb()
        {
            DragDelta += ResizeThumb_DragDelta;
        }

        void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            const int minHeight = 10;
            const int minWidth = 10;


            var grid = (Grid)Parent;
            var item = (ShapeContainer)grid.Parent;

            var dragDeltaVertical = e.VerticalChange;
            var dragDeltaHorizontal = e.HorizontalChange;

            switch (VerticalAlignment)
            {
                case VerticalAlignment.Bottom:
                    if (item.ActualHeight + dragDeltaVertical > minHeight)
                    {
                        item.Height = item.ActualHeight + dragDeltaVertical;
                    }
                    break;
                case VerticalAlignment.Top:
                    if (item.ActualHeight - dragDeltaVertical > minHeight)
                    {
                        Canvas.SetTop(item, Canvas.GetTop(item) + dragDeltaVertical);
                        item.Height = item.ActualHeight - dragDeltaVertical;
                    }
                    break;
            }

            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    if (item.ActualWidth - dragDeltaHorizontal > minWidth)
                    {
                        Canvas.SetLeft(item, Canvas.GetLeft(item) + dragDeltaHorizontal);
                        item.Width = item.ActualWidth - dragDeltaHorizontal;
                    }
                    break;
                case HorizontalAlignment.Right:
                    if (item.ActualWidth + dragDeltaHorizontal > minWidth)
                    {
                        item.Width = item.ActualWidth + dragDeltaHorizontal;
                    }
                    break;
            }

            e.Handled = true;
        }
    }
}
