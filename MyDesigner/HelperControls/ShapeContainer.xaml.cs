﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace MyDesigner.HelperControls
{
    [ContentProperty("Children")]
    public partial class ShapeContainer : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public new double Width
        {
            get { return base.Width; }
            set
            {
                base.Width = value;
                OnPropertyChanged();
            }
        }

        public new double Height
        {
            get { return base.Height; }
            set
            {
                base.Height = value;
                OnPropertyChanged();
            }
        }

        private Visibility _isSelected;
        public Visibility IsSelected {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UIElement> Children { get; private set; }

        public event Action TextChanged;

        public ShapeContainer()
        {
            IsSelected = Visibility.Collapsed;
            InitializeComponent();
            Host.DataContext = this;
            Children = new ObservableCollection<UIElement>();

            Text = "";

            Children.CollectionChanged += Children_CollectionChanged;
        }

        public string Text
        {
            get { return Label.Content.ToString(); }
            set { Label.Content = value; }
        }

        private void EditableText_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) FinishEdit(false);
            else if (e.Key == Key.Enter) FinishEdit(true);
        }

        private void EditableText_OnLostFocus(object sender, RoutedEventArgs e)
        {
            FinishEdit(true);
        }

        public void FinishEdit()
        {
            FinishEdit(true);
        }

        private void FinishEdit(bool saveChanges)
        {
            if (saveChanges) Text = EditableText.Text;

            Label.Visibility = Visibility.Visible;
            EditableText.Visibility = Visibility.Hidden;

            if (TextChanged != null) TextChanged();
        }

        public void EditText()
        {
            Label.Visibility = Visibility.Hidden;
            EditableText.Visibility = Visibility.Visible;
            EditableText.Text = Text;

            EditableText.Focus();
        }

        void Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (UIElement elem in e.NewItems)
                    {
                        Grid.SetColumn(elem, 1);
                        Grid.SetRow(elem, 1);
                        Host.Children.Insert(0, elem);
                    }

                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Move:
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:

                    foreach (UIElement elem in e.OldItems)
                    {
                        Host.Children.Remove(elem);
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    break;
            }
        }
    }
}
