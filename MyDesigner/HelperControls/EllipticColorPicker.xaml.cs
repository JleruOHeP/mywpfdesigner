﻿using System.Windows.Controls;
using Microsoft.Expression.Shapes;
using MyDesigner.Shapes;

namespace MyDesigner.HelperControls
{
    /// <summary>
    /// Interaction logic for EllipticColorPicker.xaml
    /// </summary>
    public partial class EllipticColorPicker
    {
        public MyShape ActiveShape { get; set; }
        public int ArcThickness { get; set; }

        public EllipticColorPicker()
        {
            ArcThickness = 15;

            DataContext = this;

            InitializeComponent();
        }
    }
}
