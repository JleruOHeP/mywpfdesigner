﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using MyDesigner.HelperControls;
using MyDesigner.Shapes;
using Color = System.Windows.Media.Color;
using Point = System.Windows.Point;
using Rectangle = System.Windows.Shapes.Rectangle;
using Size = System.Windows.Size;

namespace MyDesigner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MyCanvasModel Model = new MyCanvasModel();
        
        public MainWindow()
        {
            CreateMenu();
            BindModel();

            InitializeComponent();
        }

        private void BindModel()
        {
            Model.SaveRavenCommand = new ModelCommand(() =>
            {
                new RavenWrapper().Save(Model);
                Messages.Text = "Saved to Raven";
            });
            Model.LoadRavenCommand = new ModelCommand(() =>
            {
                var ravenWrapper = new RavenWrapper();
                var shapes = ravenWrapper.Load<MyShape>();
                var lines = ravenWrapper.Load<MyLine>();

                MyCanvas.Children.Clear();
                Model = new MyCanvasModel();

                ReloadShapes(shapes, lines);

                BindModel();
                Messages.Text = "Reloaded from Raven";
            });

            Model.SaveXmlCommand = new ModelCommand(() =>
            {
                var serializer = new XmlSerializer(typeof(MyCanvasModel));
                using (var writer = new StreamWriter("data.xml"))
                {
                    serializer.Serialize(writer, Model);
                    writer.Close();
                }

                Messages.Text = "Saved to Xml";
            });
            Model.LoadXmlCommand = new ModelCommand(() =>
            {
                var serializer = new XmlSerializer(typeof(MyCanvasModel));
                using (var reader = new StreamReader("data.xml"))
                {
                    var model = serializer.Deserialize(reader) as MyCanvasModel;
                    MyCanvas.Children.Clear();
                    Model = new MyCanvasModel();

                    ReloadShapes(model.Shapes, model.Lines);

                    BindModel();

                    Messages.Text = "Reloaded from Xml";
                }
            });

            DataContext = Model;
        }

        private void ReloadShapes(List<MyShape> shapes, List<MyLine> lines)
        {
            foreach (var myShape in shapes)
            {
                ShowShape(myShape);
                myShape.IsSelected = false;
            }

            foreach (var line in lines)
            {
                var reloadedStartHost = line.StartConnector.Host != null
                    ? Model.Shapes.FirstOrDefault(s => s.Id == line.StartConnector.Host.Id)
                    : null;

                var reloadedEndHost = line.EndConnector.Host != null
                    ? Model.Shapes.FirstOrDefault(s => s.Id == line.EndConnector.Host.Id)
                    : null;

                line.Reinitialize(reloadedStartHost, reloadedEndHost);

                ShowLine(line);
                ShowConnector(line.StartConnector);
                ShowConnector(line.EndConnector);

                line.EndConnector.IsSelected = false;
                line.StartConnector.IsSelected = false;
            }
        }

        #region Color picker menu
        private EllipticColorPicker _colorMenu;

        private void CreateMenu()
        {
            _colorMenu = new EllipticColorPicker();
            Panel.SetZIndex(_colorMenu, 100);
        }

        private void ShowMenu(MyShape activeShape, double mouseX, double mouseY)
        {
            MyCanvas.Children.Add(_colorMenu);

            Canvas.SetLeft(_colorMenu, mouseX - _colorMenu.Width / 2);
            Canvas.SetTop(_colorMenu, mouseY - _colorMenu.Height / 2);

            _colorMenu.ActiveShape = activeShape;
        }

        private Color? GetColor(EllipticColorPicker element)
        {
            var point = Mouse.GetPosition(element);

            element.Measure(new Size(element.Width, element.Height));
            element.Arrange(new Rect(new Size(element.Width, element.Height)));
            element.UpdateLayout();
            
            var renderTargetBitmap = new RenderTargetBitmap((int) element.ActualWidth, (int) element.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            renderTargetBitmap.Render(element);

            var centerX = element.ActualWidth/2;
            var centerY = element.ActualHeight/2;

            var radiusMax = element.ActualWidth/2;
            var radiusMin = element.ActualWidth/2 - element.ArcThickness;

            var pointRadius = Math.Sqrt( (centerX-point.X)*(centerX-point.X)+(centerY-point.Y)*(centerY-point.Y));

            if (radiusMin < pointRadius && pointRadius < radiusMax)
            {
                var croppedBitmap = new CroppedBitmap(renderTargetBitmap, new Int32Rect((int) point.X, (int) point.Y, 1, 1));

                var pixels = new byte[4];
                croppedBitmap.CopyPixels(pixels, 4, 0);

                return Color.FromArgb(255, pixels[2], pixels[1], pixels[0]);
            }

            return null;
        }

        private void MyCanvas_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var color = GetColor(_colorMenu);
            if (_colorMenu.ActiveShape != null && color != null)
            {
                _colorMenu.ActiveShape.Color = color.Value;
            }

            MyCanvas.Children.Remove(_colorMenu);
            _colorMenu.ActiveShape = null;
        }
        #endregion

        #region Show Shapes
        public void ShowShape(MyShape myShape)
        {
            Model.Shapes.Add(myShape);

            var shape = (ShapeContainer)myShape.GetShape();
            shape.MouseLeftButtonDown += shape_MouseLeftButtonDown;
            shape.MouseDoubleClick += shape_MouseDoubleClick;
            shape.MouseRightButtonDown += (s, e) => ShowMenu(myShape, e.GetPosition(MyCanvas).X, e.GetPosition(MyCanvas).Y);

            Panel.SetZIndex(shape, 0);

            MyCanvas.Children.Add(shape);
        }

        public void ShowLine(MyLine myLine)
        {
            Model.Lines.Add(myLine);

            var shape = myLine.GetShape();
            Panel.SetZIndex(shape, 10);
            MyCanvas.Children.Add(shape);
        }

        public void ShowConnector(MyConnector myConnector)
        {
            var shape = myConnector.GetShape();
            shape.MouseLeftButtonDown += shape_MouseLeftButtonDown;

            Panel.SetZIndex(shape, 11);
            MyCanvas.Children.Add(shape);
        }

        private Point? _relativeDragPosition;
        void shape_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Model.CurrentTool == Tools.Hand)
            {
                var element = (FrameworkElement)sender;
                _relativeDragPosition = e.GetPosition(element);
                
                Model.CurrentElement = element.Tag as BaseShape;

                Mouse.Capture(MyCanvas);
                e.Handled = true;
            }
        }

        void shape_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var shape = (ShapeContainer)sender;
            shape.EditText();
        }

        #endregion

        #region Drag new shapes to the canvas
        private void DragNewShape(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && sender is Shape)
            {
                var type = sender is Ellipse ? ShapeType.Ellipse
                    : sender is Rectangle ? ShapeType.Rectangle
                    : ShapeType.Triangle;

                var dragData = new DataObject("myShape", type);
                DragDrop.DoDragDrop(sender as Shape, dragData, DragDropEffects.Move);
            }
        }
        
        private void MyCanvas_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("myShape"))
            {
                var type = (ShapeType)e.Data.GetData("myShape");

                var myShape = new MyShape {ShapeType = type};
                var currentPoint = e.GetPosition(MyCanvas);
                myShape.Left = currentPoint.X;
                myShape.Top = currentPoint.Y;

                ShowShape(myShape);
            }
        }

        private void MyCanvas_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("myShape") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }
        #endregion
        
        #region Existing shapes dragging
        private void MyCanvas_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Model.CurrentElement = null;

            if (Model.CurrentTool == Tools.Line)
            {
                var startShape = GetShapeByPoint(e.GetPosition((UIElement)sender));
                var newLine = new MyLine();
                var startConnector = newLine.StartConnector;
                var endConnector = newLine.EndConnector;

                startConnector.Left = e.GetPosition(MyCanvas).X;
                startConnector.Top = e.GetPosition(MyCanvas).Y;

                if (startShape != null)
                {
                    startShape.AddConnector(startConnector);
                    startShape.AddConnector(endConnector);
                }

                ShowConnector(startConnector);
                ShowConnector(endConnector);
                ShowLine(newLine);

                Model.CurrentElement = endConnector;
                startConnector.IsSelected = true;
            }
        }

        private void MyCanvas_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(null);
            _relativeDragPosition = null;

            if (Model.CurrentElement is MyConnector)
            {
                var endShape = GetShapeByPoint(e.GetPosition((UIElement) sender));

                var connector = Model.CurrentElement as MyConnector;

                if (connector.Host != null)
                {
                    connector.Host.RemoveConnector(connector);
                }

                if (endShape != null)
                {
                    endShape.AddConnector(connector);
                }
            }
        }
        
        private void MyCanvas_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !(e.OriginalSource is ResizeThumb))
            {
                var currentPoint = e.GetPosition(MyCanvas);

                if (Model.CurrentElement != null)
                {
                    var shape = Model.CurrentElement;

                    var dragPositionX = _relativeDragPosition != null ? _relativeDragPosition.Value.X : shape.Width / 2;
                    var dragPositionY = _relativeDragPosition != null ? _relativeDragPosition.Value.Y : shape.Height / 2;

                    var newX = currentPoint.X - dragPositionX;
                    var newY = currentPoint.Y - dragPositionY;

                    if (newX < 0) newX = 0;
                    if (newX + shape.Width > MyCanvas.ActualWidth) newX = MyCanvas.ActualWidth - shape.Width;

                    if (newY < 0) newY = 0;
                    if (newY + shape.Height > MyCanvas.ActualHeight) newY = MyCanvas.ActualHeight - shape.Height;

                    shape.Left = newX;
                    shape.Top = newY;
                }
            }
        }
        #endregion

        private MyShape GetShapeByPoint(Point pt)
        {
            MyShape myShape = null;
            VisualTreeHelper.HitTest(MyCanvas, null,
                r =>
                {
                    var shape = r.VisualHit as Shape;
                    var element = shape != null && (shape.Parent is Grid)
                        ? (shape.Parent as Grid).Parent as ShapeContainer
                        : null;

                    if (element != null && element.Tag != null && element.Tag is MyShape)
                        myShape = element.Tag as MyShape;
                    return HitTestResultBehavior.Continue;
                },
                new PointHitTestParameters(pt));

            return myShape;
        }
    }
}
