﻿using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MyDesigner.Shapes
{
    public abstract class BaseShape
    {
        protected FrameworkElement Element;
        protected bool _isSelected;

        public Guid Id { get; set; }

        public FrameworkElement GetShape()
        {
            return Element;
        }

        public abstract void Delete(MyCanvasModel myCanvasModel);

        protected abstract void UpdateDependantsGeometry(double dx, double dy);

        protected double _left;
        public double Left
        {
            get { return Canvas.GetLeft(Element); }
            set
            {
                var dx = value - Left;
                Canvas.SetLeft(Element, value);
                _left = value;
                UpdateDependantsGeometry(dx, 0);
            }
        }

        protected double _top;
        public double Top
        {
            get { return Canvas.GetTop(Element); }
            set
            {
                var dy = value - Top;
                Canvas.SetTop(Element, value);
                _top = value;
                UpdateDependantsGeometry(0, dy);
            }
        }

        
        public double Width
        {
            get { return Element.Width; }
            set { Element.Width = value; }
        }

        
        public double Height
        {
            get { return Element.Height; }
            set { Element.Height = value; } 
        }
        
        protected abstract void ChangeSelection();
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                ChangeSelection();
            }
        }
    }
}
