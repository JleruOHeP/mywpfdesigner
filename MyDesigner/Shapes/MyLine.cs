﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MyDesigner.Shapes
{
    public class MyLine
    {
        public MyConnector StartConnector { get; set; }
        public MyConnector EndConnector { get; set; }

        public MyLine()
        {
            Shape = new Line();
            Shape.Fill = Brushes.Black;
            Shape.Stroke = Brushes.Black;
            Shape.StrokeThickness = 2;

            StartConnector = new MyConnector();
            StartConnector.SetLine(this);
            StartConnector.IsStart = true;

            EndConnector = new MyConnector();
            EndConnector.SetLine(this);
            EndConnector.IsStart = false;

            Id = Guid.NewGuid();
        }

        private Guid _id;

        public Guid Id
        {
            get { return _id; }
            set
            {
                _id = value;
                Shape.Tag = _id;
            }
        }

        protected Shape Shape;

        public Shape GetShape()
        {
            return Shape;
        }

        public void UpdateLineGeometry(bool startPoint)
        {
            var line = GetShape() as Line;
            if (startPoint)
            {
                line.X1 = StartConnector.CenterPoint.X;
                line.Y1 = StartConnector.CenterPoint.Y;
            }
            else
            {
                line.X2 = EndConnector.CenterPoint.X;
                line.Y2 = EndConnector.CenterPoint.Y;
            }
        }

        public void Reinitialize(MyShape startHost, MyShape endHost)
        {
            UpdateLineGeometry(true);
            UpdateLineGeometry(false);

            StartConnector.SetLine(this);
            EndConnector.SetLine(this);


            
            if (startHost != null) startHost.AddConnector(StartConnector);
            if (endHost != null) endHost.AddConnector(EndConnector);
        }
    }
}
