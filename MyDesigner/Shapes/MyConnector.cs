﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MyDesigner.Shapes
{
    public class MyConnector : BaseShape
    {
        private Brush _selectedBrush = new SolidColorBrush(Colors.HotPink);
        private Brush _defaultBrush = new SolidColorBrush(Colors.Black);
        private MyLine _line;

        public MyConnector()
        {
            var shape = new Ellipse();
            shape.Width = 10;
            shape.Height = 10;
            shape.Fill = _defaultBrush;

            Element = shape;
            Element.Tag = this;

            Canvas.SetLeft(Element, 0);
            Canvas.SetTop(Element, 0);

            Id = Guid.NewGuid();
        }

        protected override void ChangeSelection()
        {
            (Element as Shape).Fill = _isSelected ? _selectedBrush : _defaultBrush;
        }

        public Point CenterPoint { get { return new Point(Left + Width/2, Top + Height/2); } }

        public override void Delete(MyCanvasModel myCanvasModel)
        {
            var otherConnector = IsStart ? _line.EndConnector : _line.StartConnector;

            myCanvasModel.Lines.Remove(_line);
            if (otherConnector.Host != null) otherConnector.Host.RemoveConnector(otherConnector);
            if (Host != null) Host.RemoveConnector(this);

            var canvas = GetShape().Parent as Canvas;
            canvas.Children.Remove(this.GetShape());
            canvas.Children.Remove(otherConnector.GetShape());
            canvas.Children.Remove(_line.GetShape());
        }

        protected override void UpdateDependantsGeometry(double dx, double dy)
        {
            if (_line == null) return;
           _line.UpdateLineGeometry(IsStart);
        }

        public MyShape Host { get; set; }

        public bool IsStart { get; set; }

        public void SetLine(MyLine line)
        {
            _line = line;
        }
    }
}
