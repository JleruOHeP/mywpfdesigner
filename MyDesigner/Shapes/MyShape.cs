﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using MyDesigner.HelperControls;

namespace MyDesigner.Shapes
{
    public class MyShape : BaseShape
    {
        private readonly List<MyConnector> _connectors;
        private Shape _shape;
        private double _width;
        private double _height;

        public MyShape()
        {
            _connectors = new List<MyConnector>();

            _width = 100;
            _height = 100;

            var container = new ShapeContainer
            {
                Width = _width, 
                Height = _height,
                Tag = this
            };
            container.TextChanged += () => { Text = container.Text; };

            container.PropertyChanged += container_PropertyChanged;

            Element = container;
            
            ShapeType = ShapeType.Ellipse;
            Color = Colors.DodgerBlue;

            Id = Guid.NewGuid();
        }

        void container_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Width")
            {
                var scale = Width / _width;

                foreach (var connector in _connectors)
                {
                    connector.Left = Left + (connector.Left - _left) * scale;
                }

                _width = Width;
                _left = Left;
            }
            if (e.PropertyName == "Height")
            {
                var scale = Height / _height;

                foreach (var connector in _connectors)
                {
                    connector.Top = Top + (connector.Top - _top) * scale;
                }

                _height = Height;
                _top = Top;
            }
        }

        protected override void ChangeSelection()
        {
            var element = (ShapeContainer)Element;
            element.IsSelected = _isSelected ? Visibility.Visible : Visibility.Collapsed;
            if (!_isSelected) element.FinishEdit();
        }

        public void AddConnector(MyConnector connector)
        {
            _connectors.Add(connector);
            connector.Host = this;
        }

        public void RemoveConnector(MyConnector connector)
        {
            _connectors.Remove(connector);
            connector.Host = null;
        }

        public override void Delete(MyCanvasModel myCanvasModel)
        {
            myCanvasModel.Shapes.Remove(this);
            foreach (var connector in _connectors)
            {
                connector.Host = null;
            }

            var shape = GetShape();
            (shape.Parent as Canvas).Children.Remove(shape);
        }

        protected override void UpdateDependantsGeometry(double dx, double dy)
        {
            foreach (var connector in _connectors)
            {
                connector.Left += dx;
                connector.Top += dy;
            }
        }

        private ShapeType _shapeType;
        public ShapeType ShapeType
        {
            get { return _shapeType; }
            set
            {
                var container = (ShapeContainer)Element;
                container.Children.Remove(_shape);

                _shapeType = value;
                switch (_shapeType)
                {
                    case ShapeType.Ellipse:
                        _shape = new Ellipse();
                        break;
                    case ShapeType.Rectangle:
                        _shape = new Rectangle();
                        break;
                    case ShapeType.Triangle:
                        var polygon = new Polygon();
                        polygon.Points.Add(new Point(70, 70));
                        polygon.Points.Add(new Point(35, 0));
                        polygon.Points.Add(new Point(0, 70));
                        polygon.Stretch = Stretch.Uniform;
                        _shape = polygon;
                        break;
                }
                _shape.Fill = new SolidColorBrush(Color);
                _shape.Tag = this;

                container.Children.Add(_shape);
            }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                var container = (ShapeContainer)Element;
                container.Text = _text;
            }
        }

        private Color _color;
        public Color Color
        {
            get { return _color; }
            set
            {
                _shape.Fill = new SolidColorBrush(value);
                _color = value;
            }
        }
    }
}
